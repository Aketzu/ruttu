# frozen_string_literal: true

# Create bunch of events

(2000..2006).each do |y|
  Event.new(code: "#{y}01", name: "Assembly Summer #{y}").save
end

(2007..2015).each do |y|
  Event.new(code: "#{y}01", name: "Assembly Winter #{y}").save
  Event.new(code: "#{y}02", name: "Assembly Summer #{y}").save
end

(2016..2020).each do |y|
  Event.new(code: "#{y}01", name: "Assembly Winter #{y}").save
  Event.new(code: "#{y}02", name: "Assembly Summer #{y}").save
  Event.new(code: "#{y}03", name: "Assembly Winter #{y} afterparty").save
  Event.new(code: "#{y}04", name: "Assembly Summer #{y} afterparty").save
end

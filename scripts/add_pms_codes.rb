# frozen_string_literal: true

# Add PMS codes from file

current_event = Event.find_by(code: CURRENT_EVENT)

Pmscode.transaction do
  File.readlines("pms.txt").each do |pc|
    Pmscode.new(code: pc.chomp.chomp, event: current_event).save!
  end
end

# frozen_string_literal: true

# Synchronize users and stuff from LDAP to local DB

ldap = Net::LDAP.new(
  host: Rails.application.credentials.ldap_host,
  port: Rails.application.credentials.ldap_port,
  auth: {
    method: :simple,
    username: Rails.application.credentials.ldap_user,
    password: Rails.application.credentials.ldap_pass
  }
  # encryption: { method: :simple_tls }, # , tls_options: OpenSSL::SSL::SSLContext::DEFAULT_PARAMS },
)
ldap.bind # || puts('Error connecting to ldap')

# attr = ["dn", "mail", "displayName", "givenName", "sn", "cn", "telephoneNumber", "manager", "asmorgBirthday",
# "asmorgNickname", "mail", "title", "modifyTimestamp", "jpegPhoto", "memberOf", "title", "asmorgEventParticipation",
# "businessCategory", "employeeNumber"]
attr = %w[dn displayName givenName sn telephoneNumber asmorgBirthday asmorgOrganizerBeginDate asmorgTShirtSize
          asmorgNickname asmorgEventParticipation createTimestamp memberOf asmorgDietaryRequirement
          asmorgDietaryRequirementNotes street postalCode postalAddress title mail jpegPhoto departmentNumber]

events = Event.all.index_by(&:code)

User.transaction do
  nonusers = User.active.all.map(&:dn)

  ldap.search(
    base: "ou=people,dc=assembly,dc=org",
    filter: "(objectClass=inetOrgPerson)",
    attributes: attr
  ) do |entry|
    # pp entry
    dn = entry[:dn][0]
    u = User.find_by(dn: dn)
    u ||= User.new(dn: dn, created_at: Time.zone.parse(entry[:createtimestamp][0]))

    u.displayname = entry[:displayname][0]
    if u.displayname.blank?
      u.displayname = begin
        entry[:givenname][0] || ''
      rescue StandardError
        ""
      end + " " + begin
        entry[:sn][0] || ''
      rescue StandardError
        ""
      end
    end
    u.firstname = entry[:givenname][0]
    u.lastname = entry[:sn][0]
    u.phone = entry[:telephonenumber][0]
    u.birthday = begin
      Date.parse(entry[:asmorgbirthday][0])
    rescue StandardError
      nil
    end
    u.organizerbegin = begin
      Date.parse(entry[:asmorgorganizerbegindate][0])
    rescue StandardError
      nil
    end
    u.tshirtsize = entry[:asmorgtshirtsize][0]

    u.nickname = entry[:asmorgnickname][0]
    evs = []
    entry[:asmorgeventparticipation].each do |ep|
      evs << events[ep]
    end
    u.events = evs.compact

    u.diet = entry[:asmorgdietaryrequirement].join(';')
    u.dietnotes = entry[:asmorgdietaryrequirementnotes][0]

    u.street = entry[:street][0]
    u.postalcode = entry[:postalcode][0]
    u.postaladdress = entry[:postaladdress][0]
    u.title = entry[:title][0]
    u.team = entry[:departmentnumber][0]
    u.email = entry[:mail][0]
    u.photo = entry[:jpegPhoto][0]

    groups = []
    entry[:memberof].each do |grp|
      next if grp.include? 'jira-'
      next if grp.include? 'bamboo-'
      next if grp.include? 'confluence-middlemanagers'
      next if grp.include? 'confluence-external-users'

      # grp.gsub!(",ou=crews,ou=groups,dc=assembly,dc=org", '')
      # grp.gsub!(",ou=management,ou=groups,dc=assembly,dc=org", '')
      grp.gsub!(",ou=groups,dc=assembly,dc=org", '')
      grp.gsub!("cn=", '')
      groups << grp
    end
    u.groups = groups.join(";")
    u.in_ldap true
    nonusers.delete dn
    u.save
  end

  # Mark rest non-ldap users as inactive
  User.all.where(dn: nonusers).each do |u|
    u.in_ldap(false)
    u.save
  end
end

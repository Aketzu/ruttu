class InfoLog < ApplicationRecord
  belongs_to :event

  scope :event, ->(event) { where(event: event) }

  def self.states
    %w[Open Closed]
  end

  def self.stateselect
    cc = -1
    InfoLog.states.map do |s|
      cc += 1
      [s, cc]
    end
  end

  def stateicon
    %w[question check][state]
  end

  def stateclass
    %w[text-danger text-success][state]
  end

  def statetext
    InfoLog.states[state]
  end

  def eventname
    event&.shortname
  end

  def formatted_entrytime
    ActionController::Base.helpers.link_to ActionController::Base.helpers.raw(entrytime&.strftime('%a %d.%m. %H:%M')),
                                           Rails.application.routes.url_helpers.edit_info_log_path(id)
  end

  def remember_icon
    return '' unless remember == 1

    "&nbsp;&nbsp;<i class='bi bi-journal-check text-primary'></i>"
  end

  def state_with_icon
    "<i class='bi bi-#{stateicon} #{stateclass}' ></i> #{statetext}#{remember_icon}"
  end

  def contactinfo_html
    ActionController::Base.helpers.simple_format(contactinfo)
  end

  def description_html
    ActionController::Base.helpers.simple_format(description)
  end

  def solution_html
    ActionController::Base.helpers.simple_format(solution)
  end
end

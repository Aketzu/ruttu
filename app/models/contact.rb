class Contact < ApplicationRecord
  belongs_to :user

  def username=(name)
    self.user = User.where('dn ilike :dn', dn: "%#{name}%").first
  end

  def username
    return "" unless user

    user.dn
  end
end

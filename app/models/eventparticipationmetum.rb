class Eventparticipationmetum < ApplicationRecord
  belongs_to :event
  belongs_to :user

  scope :for_event, ->(event) { where(event:) }
  scope :for_user, ->(user) { where(user:) }
  scope :for_key, ->(key) { where(key:) }
end

class Invite < ApplicationRecord
  belongs_to :createdby, class_name: 'User'
  belongs_to :usedby, class_name: 'User', optional: true

  def url
    "https://halinta.assembly.org/ruttu/users/new?token=#{token}"
  end
end

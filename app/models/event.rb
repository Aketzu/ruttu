# frozen_string_literal: true

class Event < ApplicationRecord
  has_many :eventparticipations, dependent: :destroy
  has_many :eventparticipationmetum, dependent: :destroy
  has_many :users, through: :eventparticipations
  has_many :friendtickets, dependent: :destroy
  has_many :lost_items
  has_many :info_logs

  scope :recent, -> { where("code::integer > :c and name not like '%afterparty'", c: (Time.current.year - 5) * 100) }

  # TODO: Better sort
  def <=>(other)
    id <=> other.id
  end

  def shortname
    Event.shortname(code)
  end

  def afterparty?
    name.downcase.include? 'afterparty'
  end

  def self.current
    Event.find(CURRENT_EVENT_ID)
  end

  def self.revshortname(eventid)
    sn = Event.shortname(eventid)
    sn[1..2] + sn[0]
  end

  def self.shortname(eventid)
    if eventid.to_i < 100
      event = Event.find(eventid)
      code = event.code
    else
      code = eventid
    end
    yr = (code.to_i / 100) % 100
    ev = code.to_i % 100
    case ev
    when 1
      "W#{yr}"
    when 2
      "S#{yr}"
    when 5
      "G#{yr}"
    end
  end
end

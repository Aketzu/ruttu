# frozen_string_literal: true

class Pmscode < ApplicationRecord
  belongs_to :event
  belongs_to :user, optional: true

  scope :event, lambda { |ev|
    return unless ev

    where(event: ev)
  }
end

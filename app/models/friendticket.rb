# frozen_string_literal: true

class Friendticket < ApplicationRecord
  belongs_to :event
  belongs_to :user

  scope :event, lambda { |ev|
    return unless ev

    where(event: ev)
  }

  scope :search, lambda { |keyword|
    return unless keyword

    where('friendtickets.code ilike :search OR
          friendtickets.desc ilike :search OR
          users.displayname ilike :search OR
          users.nickname ilike :search',
          search: "%#{keyword}%")
      .joins(:user)
  }

  scope :user, lambda { |usr|
    return unless usr

    where(user: usr)
  }
end

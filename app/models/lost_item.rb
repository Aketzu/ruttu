class LostItem < ApplicationRecord
  belongs_to :event

  scope :event, ->(event) { where(event: event) }

  def self.states
    %w[Lost Found Closed]
  end

  def self.stateselect
    cc = -1
    LostItem.states.map do |s|
      cc += 1
      [s, cc]
    end
  end

  def stateicon
    case state
    when 0
      'question'
    when 1
      'search'
    when 2
      'check'
    end
  end

  def statetext
    LostItem.states[state]
  end

  def stateclass
    %w[text-danger text-primary text-success][state]
  end

  def eventname
    event&.shortname
  end

  def formatted_entrytime
    ActionController::Base.helpers.link_to ActionController::Base.helpers.raw(entrytime&.strftime('%a %d.%m. %H:%M')),
                                           Rails.application.routes.url_helpers.edit_lost_item_path(id)
  end

  def state_with_icon
    "<i class='bi bi-#{stateicon} #{stateclass}' ></i> #{statetext}"
  end

  def contactinfo_html
    ActionController::Base.helpers.simple_format(contactinfo)
  end

  def description_html
    ActionController::Base.helpers.simple_format(description)
  end
end

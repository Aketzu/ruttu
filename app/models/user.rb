# frozen_string_literal: true

require 'codegen'

class User < ApplicationRecord
  has_many :eventparticipations, dependent: :destroy
  has_many :eventparticipationmetum, dependent: :destroy
  has_many :events, through: :eventparticipations
  has_many :friendtickets, dependent: :destroy
  has_many :pmscodes, dependent: :destroy
  has_many :invites_created, dependent: :destroy, foreign_key: :createdby, class_name: 'Invite'
  has_many :invites_used, dependent: :destroy, foreign_key: :usedby, class_name: 'Invite'

  scope :active, -> { where(state: 1) }
  scope :nophoto, -> { select(User.column_names.map(&:to_sym) - [:photo]) }

  def <=>(other)
    displayname.downcase <=> other.displayname.downcase
  end

  def self.states
    %w[NotInLDAP Active Disabled]
  end

  def disabled?
    !state || state == 0 || state == 2
  end

  def in_ldap(found)
    return if disabled?

    self.state = found ? 1 : 0
  end

  def disable
    self.state = 2
    self.email += "-disabled"
    del_group 'confluence-users'
  end

  def enable
    self.state = 1
    self.email = email.gsub(/-disabled$/, '')
    add_group 'confluence-users'
  end

  def to_s
    displayname
  end

  def cn
    dn.split(",")[0][3..]
  end

  def eventorg?
    events.map(&:code).include? CURRENT_EVENT
  end

  def admin?
    in_group? 'people-manager'
  end

  def in_group?(grp)
    groups.include?(grp)
  end

  def del_group(grp)
    tmp = groups.split(';')
    tmp.delete grp
    self.groups = tmp.join(';')
  end

  def add_group(grp)
    tmp = groups.split(';')
    tmp << grp
    self.groups = tmp.join(';')
  end

  def access_friendtickets?
    in_group?('infocrew') || admin?
  end

  def access_infolog?
    in_group?('infolog') || admin?
  end

  def lastevent
    events.sort.reverse.each do |ev|
      next if ev.afterparty?

      return ev.shortname
    end
    ''
  end

  def allevents
    events.map(&:shortname).compact_blank.join(", ")
  end

  def in_current_event?
    events.map(&:id).include? CURRENT_EVENT_ID
  end

  def get_meta(event)
    meta = {}
    eventparticipationmetum.where(event_id: event.id, user_id: id).find_each do |m|
      meta[m.key] = JSON.parse(m.value)
    end
    meta
  end

  def set_meta(event, metas)
    update_meta(event, metas)
    eventparticipationmetum.where(event_id: event.id, user_id: id).where.not(key: metas.keys).destroy_all
  end

  def update_meta(event, metas)
    metas.each do |key, value|
      m = eventparticipationmetum.where(event_id: event.id, user_id: id, key:).first_or_create
      m.value = value.to_json
      m.save
    end
  end

  def namelink
    styledname = ERB::Util.html_escape_once displayname
    if disabled?
      styledname = "<i class='bi bi-x-lg' style='color: red' ></i> <span class='text-decoration-line-through'>#{styledname}</span>"
    end
    ActionController::Base.helpers.link_to ActionController::Base.helpers.raw(styledname),
                                           Rails.application.routes.url_helpers.edit_user_path(id)
  end

  def bitedit
    ApplicationController.render partial: 'users/togglebuttoncontainer', locals: { user: self }
  end

  include Codegen

  def add_friends
    sn = Event.shortname(CURRENT_EVENT_ID)
    daytix = 5 - friendtickets.event(CURRENT_EVENT_ID).where(desc: "#{sn} Day ticket").count
    weektix = 2 - friendtickets.event(CURRENT_EVENT_ID).where(desc: "#{sn} Weekend ticket").count

    daytix.times do
      Friendticket.new(user: self, event_id: CURRENT_EVENT_ID, code: gen_code, desc: "#{sn} Day ticket").save!
    end
    weektix.times do
      Friendticket.new(user: self, event_id: CURRENT_EVENT_ID, code: gen_code, desc: "#{sn} Weekend ticket").save!
    end
  end

  def add_pms
    p = Pmscode.all.where(event_id: CURRENT_EVENT_ID, user: nil).first
    return unless p

    p.user_id = id
    p.save
  end

  def add_orgextra
    sn = Event.shortname(CURRENT_EVENT_ID)
    Friendticket.new(user: self, event_id: CURRENT_EVENT_ID, code: gen_code, desc: "#{sn} Org weekend ticket").save!
  end

  before_save :sync_to_ldap
  after_save :check_codes
  def check_codes
    # add_friends if eventorg? && friendtickets.event(CURRENT_EVENT_ID).count < 2
    add_pms if eventorg? && pmscodes.event(CURRENT_EVENT_ID).empty?
    # add_orgextra if friendtickets.event(CURRENT_EVENT_ID).count == 0

    return unless !eventorg? && friendtickets.event(CURRENT_EVENT_ID).count > 1

    friendtickets.event(CURRENT_EVENT_ID).each do |tix|
      next if tix.redeemed_at

      tix.redeemed_at = Time.zone.at(0)
      tix.redeemed_by = "Org bit lost"
      tix.save!
    end
  end

  def self.ldap_connection
    Net::LDAP.new(
      host: Rails.application.credentials.ldap_host,
      port: Rails.application.credentials.ldap_port,
      auth: {
        method: :simple,
        username: Rails.application.credentials.ldap_user,
        password: Rails.application.credentials.ldap_pass
      }
      # encryption: { method: :simple_tls }, # , tls_options: OpenSSL::SSL::SSLContext::DEFAULT_PARAMS },
    )
  end

  def self.try_ldap_auth(user, pass)
    dn = nil
    ldap = User.ldap_connection

    ldap.search(base: "ou=people,dc=assembly,dc=org",
                filter: "(&(objectClass=inetOrgPerson)(cn=#{user}))") { |e| dn = e.dn }
    return false unless dn

    ldap.auth(dn, pass)

    if ldap.bind
      dn
    else
      false
    end
  end

  def ldapgroups
    groups.split(";").map { |x| "cn=#{x},ou=groups,dc=assembly,dc=org" }
  end

  def create_ldap_user(username, password)
    ldap = User.ldap_connection
    ldap.bind
    safe_username = username.gsub(/Ä/, 'A').gsub(/Ö/, 'Ö').gsub(/ä/, 'a').gsub(/ö/, 'o').gsub(/[^A-Za-z0-9_-]/, '')
    hashed_password = Net::LDAP::Password.generate(:ssha, password)

    self.dn = "cn=#{safe_username},ou=people,dc=assembly,dc=org"
    ldap.add(
      dn: dn,
      attributes: {
        objectClass: %w[inetOrgPerson asmorgEventOrganizer],
        cn: safe_username,
        sn: 'new',
        userPassword: hashed_password
      }
    )
    ldap.get_operation_result
  end

  def sync_to_ldap(force: false)
    ldap = User.ldap_connection
    ldap.bind
    ldap.replace_attribute dn, :asmorgEventParticipation, events.map(&:code)

    return unless force || changed?

    ldap.replace_attribute dn, :givenName, firstname if force || changed.include?('firstname')
    ldap.replace_attribute dn, :sn, lastname if force || changed.include?('lastname')
    ldap.replace_attribute dn, :asmorgNickName, nickname if force || changed.include?('nickname')
    ldap.replace_attribute dn, :displayName, displayname if force || changed.include?('displayname')
    ldap.replace_attribute dn, :departmentNumber, team if force || changed.include?('team')
    ldap.replace_attribute dn, :telephoneNumber, phone if force || changed.include?('phone')
    ldap.replace_attribute dn, :asmorgBirthday, birthday.iso8601 if birthday && (force || changed.include?('birthday'))
    if organizerbegin && (force || changed.include?('organizerbegin'))
      ldap.replace_attribute dn, :asmorgOrganizerBeginDate, organizerbegin.iso8601
    end
    ldap.replace_attribute dn, :asmorgTshirtsize, tshirtsize if force || changed.include?('tshirtsize')
    ldap.replace_attribute dn, :asmorgDietaryRequirement, diet.split(";") if diet && (force || changed.include?('diet'))
    ldap.replace_attribute dn, :asmorgDietaryRequirementNotes, dietnotes if force || changed.include?('dietnotes')
    ldap.replace_attribute dn, :street, street if force || changed.include?('street')
    ldap.replace_attribute dn, :postalCode, postalcode if force || changed.include?('postalcode')
    ldap.replace_attribute dn, :postalAddress, postaladdress if force || changed.include?('postaladdress')
    ldap.replace_attribute dn, :title, title if force || changed.include?('title')
    ldap.replace_attribute dn, :mail, email if force || changed.include?('email')
    ldap.replace_attribute dn, :jpegPhoto, photo if force || changed.include?('photo')

    # LDAP group memberships must come from group side, not from user side
    # ldap.replace_attribute dn, :memberOf, ldapgroups
    User.syncallgroups if force || changed.include?('groups')
  end

  # Free text field, can be modified
  def self.tshirtsizes
    %w[ ---
        XXS XS S M L XL XXL XXXL XXXXL
        LadyXXS LadyXS LadyS LadyM LadyL LadyXL LadyXXL LadyXXXL LadyXXXXL LadyXXXXXL]
  end

  # Coded as numbers, Do not modify or other systems break!
  def self.dietaryoptions
    cc = 0
    [
      "No lactose - laktoositon",
      "No milk - ei maitoa",
      "No peas - ei herneitä",
      "No tomato - ei tomaattia",
      "No citrus - ei sitrushedelmiä (kiivi, ananas, sitruuna, yms)",
      "No mushrooms - ei sieniä",
      "No shellfish - ei äyriäisiä",
      "No fish - ei kalaa",
      "No beans - ei papuja",
      "No nuts - ei pähkinöitä",
      "No apple - ei omenaa",
      "No carrot - ei porkkanaa",
      "No banana - ei banaania",
      "No eggs - ei kananmunaa",
      "No soy - ei soijaa",
      "No bell peppers - ei paprikaa",
      "No grains/gluten free - viljaton/gluteeniton",
      "No meat - ei lihaa"
    ].to_h do |o|
      cc += 1
      [cc, o]
    end
  end

  def dietoptions
    return "" unless diet

    diet.split(";")
  end

  def dietlist
    return [] unless diet

    diet.split(";").map { |x| User.dietaryoptions[x.to_i] }
  end

  def shortgroups
    return [] unless groups

    groups.split(";").map { |x| x.gsub(/,.*/, '') }.delete_if { |x| x == 'confluence-users' }
  end

  def self.allgroups
    [
      # Mapped to xwiki admin & users
      "confluence-administrators",
      "confluence-users",

      # General admin group
      "people-managers",

      # Access to lost&found & infolog
      "infolog",

      "marcom",
      "visitor-experience",

      "buildcrew,ou=crews",
      "compocrew,ou=crews",
      "digital-lifestyle,ou=crews",
      "esportscrew,ou=crews",
      "GamingCrew,ou=crews",
      "infocrew,ou=crews",
      "ivocrew,ou=crews",
      "livecrew,ou=crews",
      "livecrew-core,ou=crews",
      "MARCOM,ou=crews",
      "netcrew,ou=crews",
      "security,ou=crews",
      "ticketcrew,ou=crews",
      "webcrew,ou=crews",

      "content,ou=mailing-lists",
      "core,ou=mailing-lists",
      "infodesk,ou=mailing-lists",
      "lipunmyynti,ou=mailing-lists",
      "production,ou=mailing-lists",
      "program,ou=mailing-lists",

      "core,ou=management",
      "organizer-resources,ou=management",
      "production,ou=management",
      "program,ou=management"
    ]
  end

  def self.ldapgrpdn(x)
    "cn=#{x},ou=groups,dc=assembly,dc=org"
  end

  def self.allgroupmembers
    groups = {}
    User.find_each do |u|
      next unless u.groups

      u.groups.split(";").each do |g|
        groups[User.ldapgrpdn(g)] ||= []
        groups[User.ldapgrpdn(g)] << u.dn
      end
    end
    groups
  end

  def self.syncallgroups
    ldap = Net::LDAP.new(
      host: Rails.application.credentials.ldap_host,
      port: Rails.application.credentials.ldap_port,
      auth: {
        method: :simple,
        username: Rails.application.credentials.ldap_user,
        password: Rails.application.credentials.ldap_pass
      }
      # encryption: { method: :simple_tls }, # , tls_options: OpenSSL::SSL::SSLContext::DEFAULT_PARAMS },
    )

    ldap.bind

    localgroups = User.allgroupmembers
    ldapgroups = {}

    # Get current LDAP groups
    ldap.search(
      base: "ou=groups,dc=assembly,dc=org",
      filter: "(objectClass=groupOfUniqueNames)",
      attributes: %w[dn cn uniqueMember]
    ) do |entry|
      ldapgroups[entry[:dn][0]] = entry[:uniquemember]
    end

    # Add missing members
    localgroups.each do |grp, members|
      next unless ldapgroups[grp]

      diff = members - ldapgroups[grp]
      next if diff.empty?

      ops = diff.map { |m| [:add, :uniqueMember, m] }
      logger.info "#{grp}: #{ops.inspect}"
      ldap.modify dn: grp, operations: ops
    end

    # Remove extra members
    ldapgroups.each do |grp, members|
      # Some LDAP groups don't exist here (e.g. jira-users)
      next unless localgroups[grp]

      diff = members - localgroups[grp]
      next if diff.empty?

      ops = diff.map { |m| [:delete, :uniqueMember, m] }
      logger.info "#{grp}: #{ops.inspect}"
      ldap.modify dn: grp, operations: ops
    end

    true
  end
end

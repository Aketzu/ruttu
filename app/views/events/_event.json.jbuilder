# frozen_string_literal: true

json.extract! event, :id, :code, :name, :created_at, :updated_at
json.url event_url(event, format: :json)

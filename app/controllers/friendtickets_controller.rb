# frozen_string_literal: true

class FriendticketsController < ApplicationController
  skip_before_action :require_login, only: %i[conflulist]

  def conflulist
    if params[:key] != Rails.application.credentials.conflu_psk
      render json: [{ desc: "No tickets" }]
      return
    end

    u = User.where("dn ilike :dn", dn: params[:dn]).first
    u ||= User.where("dn ilike :dn", dn: "cn=#{params[:cn]},ou=people,dc=assembly,dc=org").first

    # xwiki oidc strips out "." and "^"
    u ||= User.where("replace(replace(replace(dn, '.', ''), '^', ''), '@', '') ilike :dn",
                     dn: "cn=#{params[:cn]},ou=people,dc=assembly,dc=org").first
    unless u
      render json: [{ desc: "No tickets" }]
      return
    end

    tickets = []

    u.friendtickets.event(CURRENT_EVENT_ID).order(:id).each do |tix|
      tickets << {
        description: tix.desc,
        code: tix.code,
        redeemed_at: tix.redeemed_at,
        redeemed_by: tix.redeemed_by,
        notes: tix.notes
      }
    end

    unless u.pmscodes.event(CURRENT_EVENT_ID).empty?
      tickets << { description: "Votekey", code: u.pmscodes.event(CURRENT_EVENT_ID).first.code }
    end

    render json: tickets
  end

  def search
    unless current_user.access_friendtickets?
      render text: "No permissions"
      return
    end

    if params[:redeem]
      tix = Friendticket.find(params[:redeem])
      tix.redeemed_at = Time.zone.now
      tix.redeemed_by = current_user.displayname
      tix.save
    end
    if params[:cancel]
      tix = Friendticket.find(params[:cancel])
      if (Time.now - tix.redeemed_at) > 2.minutes && !current_user.admin?
        render text: "No permissions"
        return
      end
      tix.redeemed_at = nil
      tix.redeemed_by = nil
      tix.save
    end

    return unless params[:search]

    @tickets = Friendticket.event(CURRENT_EVENT_ID).search(params[:search]).includes(:user).order("displayname",
                                                                                                  "code").all
  end

  def own
    Friendticket.transaction do
      params[:notes]&.each do |nid, txt|
        ft = Friendticket.find(nid)
        next unless ft.user == current_user

        ft.notes = txt
        ft.save
      end
    end

    @tickets = Friendticket.event(CURRENT_EVENT_ID).user(current_user).order(:id)
  end

  def stats
    unless current_user.admin?
      render text: "No permissions"
      return
    end

    res = ActiveRecord::Base.connection.execute("select \"desc\", extract(isodow from redeemed_at) as day, count(*) from friendtickets where redeemed_by != 'Org bit lost' and redeemed_at is not null group by \"desc\", extract(isodow from redeemed_at) order by 1, 2;")
    @stats = {}
    daynames = %w[Sun Mon Tue Wed Thu Fri Sat Sun]
    res.each do |r|
      event = r['desc'][0..2]
      @stats[event] ||= {}
      @stats[event][r['desc'][4..]] ||= { total: 0 }
      @stats[event][r['desc'][4..]][daynames[r['day'].to_i]] = r['count']
      @stats[event][r['desc'][4..]][:total] += r['count']
    end
  end

  def edit
    @ticket = Friendticket.find(params[:id])
  end

  def update
    unless current_user.admin?
      render text: "No permissions"
      return
    end

    @ticket = Friendticket.find(params[:id])
    if @ticket.update(params.require(:friendticket).permit(:desc, :code, :notes, :redeemed_at, :redeemed_by))
      redirect_to search_friendtickets_path, notice: "Ticket was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end
end

# frozen_string_literal: true

class SessionsController < ApplicationController
  skip_before_action :require_login, only: %i[create auth sso]
  def create; end

  def login_with_dn(dn)
    logger.info "Logging in with dn #{dn}"
    user = User.find_by('dn ILIKE ?', dn)
    unless user
      flash.alert = "User not found"
      redirect_to login_path
      return
    end
    session[:user] = user.id
    if session[:after_login]
      redirect_to session[:after_login]
      session[:after_login] = nil
    else
      redirect_to root_path
    end
  end

  def sso
    auth = request.env['omniauth.auth']
    sub = auth.dig(:extra, :raw_info, :sub)
    logger.info "Got SSO username #{sub}"

    login_with_dn("cn=#{sub},ou=people,dc=assembly,dc=org")
  end

  def auth
    dn = User.try_ldap_auth(params[:username], params[:password])
    if dn
      login_with_dn(dn)
    else
      flash.alert = "Invalid username or password"
      redirect_to login_path
    end
  end

  def logout
    session[:user] = nil
    reset_session
  end
end

class InfoLogsController < ApplicationController
  before_action :set_info_log, only: %i[show edit update destroy]

  # GET /info_logs or /info_logs.json
  def index
    unless current_user&.access_infolog?
      render text: "No permissions"
      return
    end
    @fullwidth = true

    @info_logs = InfoLog.order(entrytime: :desc)
    @events = InfoLog.select(:event_id).group(:event_id).map(&:event).map(&:shortname)

    return unless request.xhr?

    render json: @info_logs,
           only: %i[id event state remember],
           methods: %i[eventname formatted_entrytime state_with_icon contactinfo_html description_html solution_html]
  end

  # GET /info_logs/1 or /info_logs/1.json
  def show; end

  # GET /info_logs/new
  def new
    @info_log = InfoLog.new
    @info_log.entrytime = Time.current
  end

  # GET /info_logs/1/edit
  def edit
    return if current_user&.access_infolog?

    render text: "No permissions"
    nil
  end

  # POST /info_logs or /info_logs.json
  def create
    @info_log = InfoLog.new(info_log_params)
    @info_log.event_id = CURRENT_EVENT_ID

    respond_to do |format|
      if @info_log.save
        format.html { redirect_to info_logs_path, notice: "Info log was successfully created." }
        format.json { render :show, status: :created, location: @info_log }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @info_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /info_logs/1 or /info_logs/1.json
  def update
    respond_to do |format|
      if @info_log.update(info_log_params)
        format.html { redirect_to info_logs_path, notice: "Info log was successfully updated." }
        format.json { render :show, status: :ok, location: @info_log }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @info_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /info_logs/1 or /info_logs/1.json
  def destroy
    @info_log.destroy!

    respond_to do |format|
      format.html { redirect_to info_logs_path, status: :see_other, notice: "Info log was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_info_log
    @info_log = InfoLog.find(params.expect(:id))
  end

  # Only allow a list of trusted parameters through.
  def info_log_params
    params.expect(info_log: %i[state remember entrytime contactinfo description solution])
  end
end

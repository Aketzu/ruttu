class AfterpartyController < ApplicationController
  def index
    @party = Event.includes(users: [:eventparticipationmetum]).find(CURRENT_AFTERPARTY_ID)
    @usermeta = {}

    @signups = {}
    Eventparticipationmetum.for_event(@party.id).each do |m|
      @signups[m.key] ||= []
      @signups[m.key] << JSON.parse(m.value) unless m.value == 'null'
      @usermeta[m.user] ||= {}
      @usermeta[m.user][m.key] = JSON.parse(m.value) unless m.value == 'null'
    end
    @signups.each_key do |key|
      @signups[key].sort!
    end

    @meta = current_user.get_meta(@party)
    @pos = {
      party: @meta['party']&.positive? ? (@signups['party']&.index(@meta['party'])&.+ 1) : nil,
      bus1: @meta['bus1']&.positive? ? (@signups['bus1']&.index(@meta['bus1'])&.+ 1) : nil,
      bus2: @meta['bus2']&.positive? ? (@signups['bus2']&.index(@meta['bus2'])&.+ 1) : nil
    }

    @limits = {
      party: 110,
      bus1: 0,
      bus2: 0
    }

    @last = 9_999_999_999
    @last = @signups['party'][@limits[:party] - 1] if @signups['party'].length >= @limits[:party]
  end

  def signup
    @party = Event.find(CURRENT_AFTERPARTY_ID)
    if params[:participate] == '1'
      current_user.events << @party unless current_user.events.include?(@party)
      dd = current_user.get_meta(@party)
      dd['party'] ||= Time.now.to_i
      dd['bus1'] = params[:bus1] == '1' ? dd['bus1'] || Time.now.to_i : nil
      dd['bus2'] = params[:bus2] == '1' ? dd['bus2'] || Time.now.to_i : nil

      current_user.update_meta(@party, dd)
    else
      current_user.events.delete(@party)
      current_user.update_meta(@party,
                               {
                                 party: nil,
                                 bus1: nil,
                                 bus2: nil
                               })
    end
    current_user.save
    redirect_to afterparty_path
  end
end

# frozen_string_literal: true

class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?
  before_action :log_current_user
  before_action :require_login

  def log_current_user
    return unless current_user

    logger.info '  Current user: ' + current_user.to_s
  end

  def require_login
    return if logged_in?

    session[:after_login] = request.path unless request.path.include? 'logout'
    redirect_to login_path
  end

  def current_user
    @current_user ||= User.find(session[:user].to_i) unless session[:user].to_i.zero?
  end

  def logged_in?
    !!current_user
  end

  # def require_perm(perm)
  #   redirect_to root_path, alert: "You don't have permissions for that" unless logged_in? && current_user.can?(perm)
  # end
end

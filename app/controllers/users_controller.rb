require 'mimemagic'

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy toggle_event]
  skip_before_action :require_login, only: %i[mailinglists new create]

  # GET /Users or /contacts.json
  def index
    if current_user.admin?
      @users = User.nophoto.order('dn').includes(:events)
    else
      redirect_to edit_user_path(current_user)
    end

    @teams = []
    User.active.all.each do |user|
      next unless user.in_current_event?
      next unless user.team

      @teams << user.team unless @teams.include?(user.team)
    end
    @events = Event.recent.order('id desc').all.map(&:shortname)

    @fullwidth = true

    return unless request.xhr?

    render json: @users,
           only: %i[dn state displayname firstname lastname nickname team title email tshirtsize phone birthday
                    organizerbegin],
           methods: %i[namelink lastevent allevents shortgroups bitedit]
  end

  # GET /users/1 or /users/1.json
  def show; end

  # GET /users/new
  def new
    invite = Invite.find_by(token: params[:token])
    @verified = invite && invite.usedby.nil?
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  def photo
    user = User.find(params[:id])
    unless user.photo
      render status: :not_found, plain: 'Not found'
      return
    end
    mimetype = MimeMagic.by_magic(user.photo)
    response['Cache-Control'] = 'public, max-age=604800'
    send_data user.photo, type: mimetype, disposition: 'inline'
  end

  # POST /users or /users.json
  def create
    invite = Invite.find_by(token: params[:token])
    @user = User.new(state: 1, organizerbegin: Time.zone.today, displayname: '',
                     events: [Event.find(CURRENT_EVENT_ID)], groups: 'confluence-users')
    ret = @user.create_ldap_user(params[:username], params[:password])

    if !ret || ret[:code] != 0
      @user.errors.add(:base, "User creation failed: #{ret[:message]}")
      render :new, status: :unprocessable_entity
      return
    end

    respond_to do |format|
      if @user.save
        # Log in automatically
        invite.usedby = @user
        invite.save!

        session[:user] = @user.id
        format.html { redirect_to edit_user_path(@user), notice: "User account created. Please fill in details." }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    unless current_user == @user || current_user.admin?
      render text: "No permissions"
      return
    end

    # params[:user][:diet] = params[:user][:diet].reject(&:empty?).join(";") if params[:user][:diet].is_a? Array

    params[:user][:photo] = params[:user][:photo].read if params[:user][:photo]
    params[:user][:diet] = params[:user][:diet].keys.join(";") if params[:user][:diet]

    if current_user.admin?
      if params[:user][:events]
        events = Event.where(code: params[:user][:events].keys)
        @user.events = events
        @user.save
      end
      params[:user][:groups] = params[:user][:groups].keys.join(";") if params[:user][:groups]

    end

    respond_to do |format|
      if @user.update(users_params)

        # Disable/enable edits groups so it must happen after .update (since it also updates groups)
        if @current_user.admin?
          @user.disable if params[:commit] == 'Disable user'
          @user.enable if params[:commit] == 'Enable user'
          @user.save
        end

        @user.sync_to_ldap(force: true)
        notice = 'User was successfully updated.'
        if @user.created_at < 15.minutes.ago
          notice = 'Thank you for filling in the details. Please contact you recruiter for user account verification.'
        end
        format.html { redirect_to user_url(@user), notice: notice }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1 or /users/1.json
  def destroy
    unless current_user.admin?
      render text: "No permissions"
      return
    end
    @user.destroy!

    respond_to do |format|
      format.html { redirect_to users_url, notice: "User was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def toggle_event
    @user.events.delete(Event.current) if params[:remove] == 'true'
    @user.events << Event.current if params[:add] == 'true'
    @user.save
  end

  def mailinglists
    unless request.headers[:authorization] == Rails.application.credentials.mailinglist_auth
      render plain: "Invalid auth", status: :unauthorized
      return
    end

    @lists = {}
    listname_exceptions = {
      "core" => "ydin",
      "livecrew" => "live-crew",
      "livecrew-core" => "live-core",
      "webcrew" => "web"
    }

    User.active.nophoto.find_each do |user|
      next unless user.email

      @lists['organizers'] ||= []
      @lists['organizers'] << user.email

      user.groups.split(';').each do |group|
        next if %w[confluence-users marcom].include?(group)

        group.gsub!(/,.*/, "")
        group.gsub!(/[^A-Za-z0-9-]/, "")
        group = listname_exceptions[group] if listname_exceptions[group]
        @lists[group] ||= []
        @lists[group] << user.email
      end
    end
    render plain: @lists.map { |l, v| v.map { |e| "#{l},#{e}" } }.flatten.join("\n")
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    params[:id] = current_user.id if params[:id] == 'self'
    @user = User.find(params[:id])
    return if current_user == @user || current_user.admin?

    redirect_to users_url,
                notice: "No permissions"
  end

  # Only allow a list of trusted parameters through.
  def users_params
    allowed = %i[
      firstname lastname displayname nickname phone birthday organizerbegin tshirtsize diet dietnotes street postalcode postaladdress email photo
    ]
    allowed += %i[events groups team title] if current_user.admin?
    params.require(:user).permit(allowed)
  end
end

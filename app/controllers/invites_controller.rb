class InvitesController < ApplicationController
  before_action :set_invite, only: %i[show edit update destroy]

  # GET /invites or /invites.json
  def index
    unless current_user.admin?
      render text: "No permissions"
      return
    end

    @invites = Invite.all
  end

  # GET /invites/1 or /invites/1.json
  def show; end

  include Codegen
  # GET /invites/new
  def new
    unless current_user == @user || current_user.admin?
      render text: "No permissions"
      return
    end
    @invite = Invite.new(createdby: current_user, token: gen_code)

    respond_to do |format|
      if @invite.save
        format.html { redirect_to invites_path, notice: "Invite was successfully created. Address: #{@invite.url}" }
        format.json { render :show, status: :created, location: @invite }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /invites/1/edit
  def edit; end

  # POST /invites or /invites.json
  def create
    @invite = Invite.new(invite_params)

    respond_to do |format|
      if @invite.save
        format.html { redirect_to @invite, notice: "Invite was successfully created." }
        format.json { render :show, status: :created, location: @invite }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invites/1 or /invites/1.json
  def update
    respond_to do |format|
      if @invite.update(invite_params)
        format.html { redirect_to @invite, notice: "Invite was successfully updated." }
        format.json { render :show, status: :ok, location: @invite }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invites/1 or /invites/1.json
  def destroy
    @invite.destroy!

    respond_to do |format|
      format.html { redirect_to invites_path, status: :see_other, notice: "Invite was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_invite
    @invite = Invite.find(params.expect(:id))
  end

  # Only allow a list of trusted parameters through.
  def invite_params
    params.expect(invite: %i[token createdby usedby])
  end
end

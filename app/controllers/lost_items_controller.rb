class LostItemsController < ApplicationController
  before_action :set_lost_item, only: %i[show edit update destroy]

  # GET /lost_items or /lost_items.json
  def index
    unless current_user&.access_infolog?
      render text: "No permissions"
      return
    end
    @fullwidth = true
    @lost_items = LostItem.order(entrytime: :desc)

    @events = LostItem.select(:event_id).group(:event_id).map(&:event).map(&:shortname)

    return unless request.xhr?

    render json: @lost_items,
           only: %i[id event state],
           methods: %i[eventname formatted_entrytime state_with_icon contactinfo_html description_html]
  end

  # GET /lost_items/1 or /lost_items/1.json
  def show; end

  # GET /lost_items/new
  def new
    @lost_item = LostItem.new
    @lost_item.entrytime = Time.current
  end

  # GET /lost_items/1/edit
  def edit
    return if current_user&.access_infolog?

    render text: "No permissions"
    nil
  end

  # POST /lost_items or /lost_items.json
  def create
    @lost_item = LostItem.new(lost_item_params)
    @lost_item.event_id = CURRENT_EVENT_ID

    respond_to do |format|
      if @lost_item.save
        format.html { redirect_to lost_items_path, notice: "Lost item was successfully created." }
        format.json { render :show, status: :created, location: @lost_item }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @lost_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lost_items/1 or /lost_items/1.json
  def update
    respond_to do |format|
      if @lost_item.update(lost_item_params)
        format.html { redirect_to lost_items_path, notice: "Lost item was successfully updated." }
        format.json { render :show, status: :ok, location: @lost_item }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @lost_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lost_items/1 or /lost_items/1.json
  def destroy
    @lost_item.destroy!

    respond_to do |format|
      format.html { redirect_to lost_items_path, status: :see_other, notice: "Lost item was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_lost_item
    @lost_item = LostItem.find(params.expect(:id))
  end

  # Only allow a list of trusted parameters through.
  def lost_item_params
    params.expect(lost_item: %i[state entrytime location contactinfo description])
  end
end

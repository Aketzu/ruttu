# frozen_string_literal: true

Rails.application.routes.draw do
  resources :invites
  resources :info_logs
  resources :lost_items
  get 'peliluola', as: 'afterparty', action: 'index', controller: 'afterparty'
  post 'peliluola/signup', as: 'signup_afterparty', controller: 'afterparty', action: 'signup'

  resources :contacts do
    collection do
      get 'export'
    end
  end

  resources :users do
    collection do
      get 'mailinglists'
    end
    member do
      get 'photo'
      post 'toggle_event'
    end
  end

  get "sessions/create", as: "login"
  post "sessions/auth", as: "auth"
  get "sessions/logout", as: "logout"
  post "sessions/logout"
  get 'oidc/authenticator/callback', to: 'sessions#sso'

  # a-umlaut in querystring just fails...
  get "friendtickets/conflulist"
  # so putting it in path works...
  get "friendtickets/conflulist/:cn", controller: 'friendtickets', action: 'conflulist'

  get "friendtickets/search", as: "search_friendtickets"
  post "friendtickets/search"

  get "friendtickets/own"
  post "friendtickets/own"

  get "friendtickets/edit"
  patch "friendtickets/:id", controller: 'friendtickets', as: 'friendticket', action: 'update'

  get "friendtickets/stats"

  resources :events do
    member do
      get 'stats'
      get 'organizers'
      get 'allergies'
    end
  end

  root "users#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

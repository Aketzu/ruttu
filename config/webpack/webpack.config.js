// See the shakacode/shakapacker README and docs directory for advice on customizing your webpackConfig.
const { generateWebpackConfig, merge } = require('shakapacker')

const options = {
  resolve: {
    extensions: ['.css'],
    fallback: {
      // https://github.com/hhurz/tableExport.jquery.plugin/issues/339
      fs: false
    }
  },
  ignoreWarnings: [
    { module: /x-editable/ }
  ],
  module: {
    rules: [
//        {
//          test: /\.s[ac]ss$/,
//            use: [
//              'style-loader',
//              'css-loader',
//              'resolve-url-loader',
//              'sass-loader',
//            ]
//        },
//      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'file-loader', options: { name: '[name].[ext]' } },
    ]
  }
};

const webpackConfig = generateWebpackConfig(options)

module.exports = merge(options, webpackConfig)

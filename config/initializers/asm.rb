# frozen_string_literal: true

CURRENT_EVENT = "202501"
CURRENT_EVENT_ID = 63

CURRENT_AFTERPARTY_ID = 65

# CURRENT_EVENT_ID = Event.find_by_code(CURRENT_EVENT).id

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :keycloak_openid, Rails.application.credentials.client_id, Rails.application.credentials.client_secret,
           client_options: { site: 'https://auth.assembly.org', realm: 'assembly', base_url: '' },
           callback_path: '/ruttu/oidc/authenticator/callback',
           name: 'keycloak'
end

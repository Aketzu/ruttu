# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2025_02_22_174633) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string "title"
    t.bigint "user_id", null: false
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id"
  end

  create_table "eventparticipationmeta", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "user_id", null: false
    t.string "key"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id", "user_id", "key"], name: "index_eventparticipationmeta_on_event_id_and_user_id_and_key", unique: true
    t.index ["event_id"], name: "index_eventparticipationmeta_on_event_id"
    t.index ["user_id"], name: "index_eventparticipationmeta_on_user_id"
  end

  create_table "eventparticipations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "event_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["event_id"], name: "index_eventparticipations_on_event_id"
    t.index ["user_id"], name: "index_eventparticipations_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "friendtickets", force: :cascade do |t|
    t.bigint "event_id"
    t.bigint "user_id"
    t.string "code"
    t.string "desc"
    t.datetime "redeemed_at", precision: nil
    t.string "redeemed_by"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "notes"
    t.index ["code"], name: "index_friendtickets_on_code"
    t.index ["event_id"], name: "index_friendtickets_on_event_id"
    t.index ["user_id"], name: "index_friendtickets_on_user_id"
  end

  create_table "info_logs", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.integer "state"
    t.integer "remember"
    t.datetime "entrytime"
    t.string "contactinfo"
    t.text "description"
    t.text "solution"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_info_logs_on_event_id"
  end

  create_table "invites", force: :cascade do |t|
    t.string "token"
    t.bigint "createdby_id"
    t.bigint "usedby_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["createdby_id"], name: "index_invites_on_createdby_id"
    t.index ["usedby_id"], name: "index_invites_on_usedby_id"
  end

  create_table "lost_items", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.integer "state"
    t.datetime "entrytime"
    t.string "location"
    t.string "contactinfo"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_lost_items_on_event_id"
  end

  create_table "pmscodes", force: :cascade do |t|
    t.string "code"
    t.bigint "event_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["code"], name: "index_pmscodes_on_code", unique: true
    t.index ["event_id"], name: "index_pmscodes_on_event_id"
    t.index ["user_id"], name: "index_pmscodes_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "dn"
    t.string "displayname"
    t.string "nickname"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "firstname"
    t.string "lastname"
    t.text "groups"
    t.string "phone"
    t.date "birthday"
    t.date "organizerbegin"
    t.string "tshirtsize"
    t.string "diet"
    t.text "dietnotes"
    t.string "street"
    t.string "postalcode"
    t.string "postaladdress"
    t.string "title"
    t.string "email"
    t.binary "photo"
    t.string "team"
    t.integer "state"
    t.index ["dn"], name: "index_users_on_dn", unique: true
  end

  add_foreign_key "contacts", "users"
  add_foreign_key "eventparticipationmeta", "events"
  add_foreign_key "eventparticipationmeta", "users"
  add_foreign_key "eventparticipations", "events"
  add_foreign_key "eventparticipations", "users"
  add_foreign_key "friendtickets", "events"
  add_foreign_key "friendtickets", "users"
  add_foreign_key "info_logs", "events"
  add_foreign_key "invites", "users", column: "createdby_id"
  add_foreign_key "invites", "users", column: "usedby_id"
  add_foreign_key "lost_items", "events"
  add_foreign_key "pmscodes", "events"
  add_foreign_key "pmscodes", "users"
end

# frozen_string_literal: true

class CreateInvites < ActiveRecord::Migration[8.0]
  def change
    create_table :invites do |t|
      t.string :token
      t.references :createdby, foreign_key: { to_table: 'users', foreign_key: 'id' }
      t.references :usedby, foreign_key: { to_table: 'users', foreign_key: 'id' }

      t.timestamps
    end
  end
end

class CreateInfoLogs < ActiveRecord::Migration[8.0]
  def change
    create_table :info_logs do |t|
      t.references :event, null: false, foreign_key: true
      t.integer :state
      t.integer :remember
      t.datetime :entrytime
      t.string :contactinfo
      t.text :description
      t.text :solution

      t.timestamps
    end
  end
end

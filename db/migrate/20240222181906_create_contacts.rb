class CreateContacts < ActiveRecord::Migration[7.1]
  def change
    create_table :contacts do |t|
      t.string :title
      t.references :user, null: false, foreign_key: true
      t.string :phone

      t.timestamps
    end
  end
end

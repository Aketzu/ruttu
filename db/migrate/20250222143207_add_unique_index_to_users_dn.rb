class AddUniqueIndexToUsersDn < ActiveRecord::Migration[8.0]
  def change
    remove_index :users, :dn
    add_index :users, :dn, unique: true
  end
end

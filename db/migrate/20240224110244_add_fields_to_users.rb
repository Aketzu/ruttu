class AddFieldsToUsers < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :diet, :string
    add_column :users, :dietnotes, :text
    add_column :users, :street, :string
    add_column :users, :postalcode, :string
    add_column :users, :postaladdress, :string
    add_column :users, :title, :string
    add_column :users, :email, :string
    add_column :users, :photo, :binary
  end
end

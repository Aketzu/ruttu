# frozen_string_literal: true

class CreatePmscodes < ActiveRecord::Migration[5.2]
  def change
    create_table :pmscodes do |t|
      t.string :code
      t.references :event, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :pmscodes, :code, unique: true
  end
end

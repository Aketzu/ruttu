# frozen_string_literal: true

class CreateFriendtickets < ActiveRecord::Migration[5.2]
  def change
    create_table :friendtickets do |t|
      t.references :event, foreign_key: true
      t.references :user, foreign_key: true
      t.string :code
      t.string :desc
      t.datetime :redeemed_at
      t.string :redeemed_by

      t.timestamps
    end
    add_index :friendtickets, :code
  end
end

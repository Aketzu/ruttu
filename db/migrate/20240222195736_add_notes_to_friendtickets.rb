class AddNotesToFriendtickets < ActiveRecord::Migration[7.1]
  def change
    add_column :friendtickets, :notes, :string
  end
end

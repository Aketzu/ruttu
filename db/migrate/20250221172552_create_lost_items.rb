class CreateLostItems < ActiveRecord::Migration[8.0]
  def change
    create_table :lost_items do |t|
      t.references :event, null: false, foreign_key: true
      t.integer :state
      t.datetime :entrytime
      t.string :location
      t.string :contactinfo
      t.text :description

      t.timestamps
    end
  end
end

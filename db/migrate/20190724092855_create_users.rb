# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :dn
      t.string :displayname
      t.string :nickname

      t.timestamps
    end
    add_index :users, :dn
  end
end

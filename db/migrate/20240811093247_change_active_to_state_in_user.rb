class ChangeActiveToStateInUser < ActiveRecord::Migration[7.1]
  def change
    change_table :users, bulk: true do |t|
      t.integer :state
      t.remove :active, type: :boolean
    end
  end
end

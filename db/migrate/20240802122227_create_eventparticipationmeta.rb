# frozen_string_literal: true

class CreateEventparticipationmeta < ActiveRecord::Migration[7.1]
  def change
    create_table :eventparticipationmeta do |t|
      t.references :event, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :key
      t.string :value

      t.timestamps
    end
    add_index :eventparticipationmeta, %i[event_id user_id key], unique: true
  end
end

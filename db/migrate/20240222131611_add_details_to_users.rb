class AddDetailsToUsers < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :groups, :text
    add_column :users, :phone, :string
    add_column :users, :birthday, :date
    add_column :users, :organizerbegin, :date
    add_column :users, :tshirtsize, :string
  end
end

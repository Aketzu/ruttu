require "application_system_test_case"

class InfoLogsTest < ApplicationSystemTestCase
  setup do
    @info_log = info_logs(:one)
  end

  test "visiting the index" do
    visit info_logs_url
    assert_selector "h1", text: "Info logs"
  end

  test "should create info log" do
    visit info_logs_url
    click_on "New info log"

    fill_in "Contactinfo", with: @info_log.contactinfo
    fill_in "Description", with: @info_log.description
    fill_in "Entrytime", with: @info_log.entrytime
    fill_in "Event", with: @info_log.event_id
    fill_in "Remember", with: @info_log.remember
    fill_in "Solution", with: @info_log.solution
    fill_in "State", with: @info_log.state
    click_on "Create Info log"

    assert_text "Info log was successfully created"
    click_on "Back"
  end

  test "should update Info log" do
    visit info_log_url(@info_log)
    click_on "Edit this info log", match: :first

    fill_in "Contactinfo", with: @info_log.contactinfo
    fill_in "Description", with: @info_log.description
    fill_in "Entrytime", with: @info_log.entrytime.to_s
    fill_in "Event", with: @info_log.event_id
    fill_in "Remember", with: @info_log.remember
    fill_in "Solution", with: @info_log.solution
    fill_in "State", with: @info_log.state
    click_on "Update Info log"

    assert_text "Info log was successfully updated"
    click_on "Back"
  end

  test "should destroy Info log" do
    visit info_log_url(@info_log)
    click_on "Destroy this info log", match: :first

    assert_text "Info log was successfully destroyed"
  end
end

# frozen_string_literal: true

require "test_helper"

class FriendticketsControllerTest < ActionDispatch::IntegrationTest
  test "should get conflulist" do
    get friendtickets_conflulist_url
    assert_response :success
  end
end

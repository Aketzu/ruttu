require "test_helper"

class InfoLogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @info_log = info_logs(:one)
  end

  test "should get index" do
    get info_logs_url
    assert_response :success
  end

  test "should get new" do
    get new_info_log_url
    assert_response :success
  end

  test "should create info_log" do
    assert_difference("InfoLog.count") do
      post info_logs_url, params: { info_log: { contactinfo: @info_log.contactinfo, description: @info_log.description, entrytime: @info_log.entrytime, event_id: @info_log.event_id, remember: @info_log.remember, solution: @info_log.solution, state: @info_log.state } }
    end

    assert_redirected_to info_log_url(InfoLog.last)
  end

  test "should show info_log" do
    get info_log_url(@info_log)
    assert_response :success
  end

  test "should get edit" do
    get edit_info_log_url(@info_log)
    assert_response :success
  end

  test "should update info_log" do
    patch info_log_url(@info_log), params: { info_log: { contactinfo: @info_log.contactinfo, description: @info_log.description, entrytime: @info_log.entrytime, event_id: @info_log.event_id, remember: @info_log.remember, solution: @info_log.solution, state: @info_log.state } }
    assert_redirected_to info_log_url(@info_log)
  end

  test "should destroy info_log" do
    assert_difference("InfoLog.count", -1) do
      delete info_log_url(@info_log)
    end

    assert_redirected_to info_logs_url
  end
end

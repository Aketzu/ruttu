require "test_helper"

class AfterpartyControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get afterparty_index_url
    assert_response :success
  end

  test "should get signup" do
    get afterparty_signup_url
    assert_response :success
  end
end

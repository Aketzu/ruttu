# frozen_string_literal: true

require 'concode'
require 'securerandom'
require 'base64'

def getsimplecode
  SecureRandom.base64(20).gsub(/[^A-Za-z0-9]/, '')
end

shortev = Event.shortname(CURRENT_EVENT_ID).downcase

# Org
Pmscode.transaction do
  500.times do
    code = "org#{shortev}#{getsimplecode[0..15]}"
    Pmscode.new(code: code, event_id: CURRENT_EVENT_ID).save!
  end
end

visitorcode = []
500.times do
  visitorcode << (shortev + getsimplecode[0..10])
end

File.write("compo-visitorcode-#{shortev}.txt", visitorcode.join("\n"))

orgcode = Pmscode.all.where(event_id: CURRENT_EVENT_ID).map(&:code)
File.write("compo-orgcode-#{shortev}.txt", orgcode.join("\n"))

influencercode = Friendticket.all.where(event_id: CURRENT_EVENT_ID).map(&:code)
File.write("compo-influencercode-#{shortev}.txt", influencercode.join("\n"))
